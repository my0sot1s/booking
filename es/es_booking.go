package es

import (
	"encoding/json"

	"github.com/olivere/elastic"
	"gitlab.com/my0sot1s/header/seat"
)

const (
	_booking = "esbooking"
	_type    = "_doc"
)

// SearchIndexBranchs ss
func (s *SearchMachine) SearchIndexBooking(query *seat.BookingRequest) ([]*seat.Booking, error) {
	esQuery := query.GetEsQuery()
	termQuery := elastic.NewTermQuery(esQuery.GetKey(), esQuery.GetQuery())
	size := int(esQuery.GetSize())
	if size == 0 {
		size = 40
	}
	searchResult, err := s.client.Search().
		Index(_booking).
		Query(termQuery).
		From(int(esQuery.GetFrom())).
		Size(size).
		Do(s.ctx)
	if err != nil {
		return nil, err
	}
	bookings := make([]*seat.Booking, 0)
	if searchResult.Hits.TotalHits > 0 {
		// Iterate through results
		for _, hit := range searchResult.Hits.Hits {
			// hit.Index contains the name of the index
			var booking *seat.Booking
			// Deserialize hit.Source into a Tweet (could also be just a map[string]interface{}).
			err := json.Unmarshal(*hit.Source, &booking)
			if err == nil {
				bookings = append(bookings, booking)
			}
		}
	}
	return bookings, nil
}

// AddIndexBranch ss
func (s *SearchMachine) AddIndexBooking(booking *seat.Booking) error {
	_, err := s.client.Index().
		Index(_booking).
		Type(_type).
		Id(booking.GetId()).
		BodyJson(booking).
		Do(s.ctx)
	return err
}

// UpdateIndexBranch any
func (s *SearchMachine) UpdateIndexService(booking *seat.Booking) error {
	_, err := s.client.Update().
		Index(_booking).
		Type(_type).
		Id(booking.GetId()).
		Doc(&booking).Do(s.ctx)
	return err
}

// DeleteIndexBranch any
func (s *SearchMachine) DeleteIndexBranch(id string) error {
	_, err := s.client.Delete().
		Index(_booking).
		Type(_type).
		Id(id).
		Do(s.ctx)
	return err
}
