package es

import (
	// "github.com/elastic/go-elasticsearch"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"syscall"
	"time"

	"github.com/olivere/elastic"
)

// SearchMachine make machine for search
type SearchMachine struct {
	client *elastic.Client
	ctx    context.Context
}
type esRetries struct {
	backoff elastic.Backoff
}

func seRetrier() *esRetries {
	return &esRetries{
		backoff: elastic.NewExponentialBackoff(100*time.Millisecond, 8*time.Second),
	}
}

func (r *esRetries) Retry(ctx context.Context, retry int, req *http.Request, resp *http.Response, err error) (time.Duration, bool, error) {
	// Fail hard on a specific error
	if err == syscall.ECONNREFUSED {
		return 0, false, errors.New("Elasticsearch or network down")
	}
	// Stop after 5 retries
	if retry >= 5 {
		return 0, false, nil
	}
	// Let the backoff strategy decide how long to wait and whether to stop
	wait, stop := r.backoff.Next(retry)
	return wait, stop, nil
}

// InitElastic start elastic
func (s *SearchMachine) InitElastic(url, nameIndex, mappingURL string) error {
	s.ctx = context.Background()
	client, err := elastic.NewClient(
		elastic.SetURL(url),
		elastic.SetSniff(false),
		elastic.SetRetrier(seRetrier()),
	)

	if err != nil {
		return err
	}
	s.client = client

	if _, _, err := client.Ping(url).Do(s.ctx); err != nil {
		return err
	}

	// createIndex
	dat, err := ioutil.ReadFile(mappingURL)

	if err != nil {
		return err
	}
	return s.injectIndex(nameIndex, string(dat))
}

// InitElasticConfig start elastic - [_index]: pathMapping
func (s *SearchMachine) InitElasticConfig(url string, configs map[string]string) []error {
	s.ctx = context.Background()
	client, err := elastic.NewClient(
		elastic.SetURL(url),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false),
		elastic.SetRetrier(seRetrier()),
	)

	if err != nil {
		return []error{err}
	}
	s.client = client

	if _, _, err := client.Ping(url).Do(s.ctx); err != nil {
		return []error{err}
	}
	errs := make([]error, 0)
	for nameIndex, mappingURL := range configs {
		// createIndex
		dat, err := ioutil.ReadFile(mappingURL)
		if err != nil {
			errs = append(errs, err)
			continue
		}
		if err := s.injectIndex(nameIndex, string(dat)); err != nil {
			errs = append(errs, err)
		}
	}
	return errs
}

// createIndex
func (s *SearchMachine) injectIndex(nameIndex, mapping string) error {
	exists, err := s.client.IndexExists(nameIndex).Do(context.Background())
	if err != nil {
		return err
	}

	if exists {
		return nil
	}

	res, err := s.client.
		CreateIndex(nameIndex).
		BodyString(mapping).
		Do(s.ctx)

	if err != nil {
		return err
	}

	if !res.Acknowledged {
		return errors.New(`CreateIndex was not acknowledged.
		Check that timeout value is correct.`)
	}
	return nil
}

// DeleteIndex any
func (s *SearchMachine) DeleteIndex(index string) error {
	_, err := s.client.DeleteIndex(index).
		Do(s.ctx)
	return err
}
