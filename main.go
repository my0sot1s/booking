package main

import (
	"os"

	"github.com/urfave/cli"
	"gitlab.com/my0sot1s/helper"
)

func main() {
	helper.Log(helper.TitleConsole("H e l l o   b o o k i n g"))
	if err := startApp(configApp()); err != nil {
		helper.ErrLog(err)
		panic(err)
	}
}
func configApp() *cli.App {
	app := cli.NewApp()
	app.Action = func(c *cli.Context) error {
		helper.Log("Wow, Do you know my name??")
		return nil
	}
	return app
}
func startApp(app *cli.App) error {
	app.Commands = []cli.Command{
		{
			Name:   "start",
			Action: func(ctx *cli.Context) { InitGrpc(":" + os.Getenv("PORT")) },
		},
	}
	return app.Run(os.Args)
}
