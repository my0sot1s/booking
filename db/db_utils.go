package db

import (
	"gitlab.com/my0sot1s/helper"
	"gopkg.in/mgo.v2/bson"
)

var oh = bson.ObjectIdHex

func limitAnchor(limit int, anchor string) helper.M {
	q := helper.M{}
	if anchor == "" {
		return q
	}
	if limit < 0 {
		q["_id"] = helper.M{"$lt": oh(anchor)}
	} else {
		q["_id"] = helper.M{"$gt": oh(anchor)}
	}
	return q
}
func convtId(m helper.M) helper.M {
	m["id"] = m["_id"]
	delete(m, "_id")
	return m
}
func convtSliceId(s []helper.M) []helper.M {
	n := make([]helper.M, 0)
	for _, v := range s {
		n = append(n, convtId(v))
	}
	return n
}
