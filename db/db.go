package db

import (
	"errors"

	"gitlab.com/my0sot1s/header/seat"
	"gitlab.com/my0sot1s/helper"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type DB struct {
	db  *mgo.Database
	url string
}

const (
	Customer = "Customer"
	Booking  = "Booking"
)

func (d *DB) InitDB(dbName, url string) error {
	session, err := mgo.Dial(url)
	if err != nil {
		return err
	}
	session.SetMode(mgo.Monotonic, true)
	d.db = session.DB(dbName)
	return nil
}

// --------- Customer ----------------
func (d *DB) ReadCustomer(query *seat.CustomerRequest) (*seat.Customer, error) {
	s, m := &seat.Customer{}, make(helper.M)
	if query.GetId() != "" {
		if !bson.IsObjectIdHex(query.GetId()) {
			return nil, errors.New("id is not hex")
		}
		if err := d.db.C(Customer).FindId(bson.ObjectIdHex(query.GetId())).One(&m); err != nil {
			return nil, err
		}
		err := helper.ConveterM2I(convtId(m), &s)
		return s, err
	} else if query.GetPhone() != "" {
		if err := d.db.C(Customer).Find(bson.M{"phone": query.GetPhone()}).One(&m); err != nil {
			return nil, err
		}
		err := helper.ConveterM2I(convtId(m), &s)
		return s, err
	}
	return nil, errors.New("not have id or phone")
}

func (d *DB) ReadCustomers(query *seat.CustomerRequest) ([]*seat.Customer, error) {
	m, customers := make([]helper.M, 0), make([]*seat.Customer, 0)
	anchor, limit := query.GetAnchor(), int(query.GetLimit())
	if err := d.db.C(Customer).Find(limitAnchor(limit, anchor)).Limit(limit).All(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtSliceId(m), &customers)
	return customers, err
}

func (d *DB) UpsertCustomer(cus *seat.Customer) (*seat.Customer, error) {
	if !bson.IsObjectIdHex(cus.GetId()) {
		m, bid := make(bson.M), bson.NewObjectId()
		helper.ConveterI2M(cus, m)
		m["_id"], cus.Id = bid, bid.Hex()
		return cus, d.db.C(Customer).Insert(m)
	}
	cus.Created = 0
	m, bid := make(bson.M), bson.ObjectIdHex(cus.GetId())
	helper.ConveterI2M(cus, m)
	return cus, d.db.C(Customer).UpdateId(bid, bson.M{"$set": m})
}

func (d *DB) DeleteCustomer(id, accountId string) error {
	if !bson.IsObjectIdHex(id) {
		return errors.New("id is not hex")
	}
	return d.db.C(Customer).Remove(bson.M{"_id": bson.ObjectIdHex(id), "account_id": accountId})
}

// --------- Booking ----------------
func (d *DB) ReadBooking(query *seat.BookingRequest) (*seat.Booking, error) {
	booking, m := &seat.Booking{}, make(helper.M)
	if query.GetId() != "" {
		if !bson.IsObjectIdHex(query.GetId()) {
			return nil, errors.New("id is not hex")
		}
		if err := d.db.C(Booking).FindId(bson.ObjectIdHex(query.GetId())).One(&m); err != nil {
			return nil, err
		}
		err := helper.ConveterM2I(convtId(m), &booking)
		return booking, err
	} else if query.GetCustomerId() != "" {
		if err := d.db.C(Customer).Find(bson.M{"_id": query.GetCustomerId()}).One(&m); err != nil {
			return nil, err
		}
		err := helper.ConveterM2I(convtId(m), &booking)
		return booking, err
	}
	return nil, errors.New("not have id or customer_id")
}

func (d *DB) ReadBookings(query *seat.BookingRequest) ([]*seat.Booking, error) {
	m, bookings := make([]helper.M, 0), make([]*seat.Booking, 0)
	anchor, limit := query.GetAnchor(), int(query.GetLimit())
	if err := d.db.C(Booking).Find(limitAnchor(limit, anchor)).Limit(limit).All(&m); err != nil {
		return nil, err
	}
	err := helper.ConveterM2I(convtSliceId(m), &bookings)
	return bookings, err
}

func (d *DB) UpsertBooking(b *seat.Booking) (*seat.Booking, error) {
	if !bson.IsObjectIdHex(b.GetId()) {
		m, bid := make(bson.M), bson.NewObjectId()
		helper.ConveterI2M(b, m)
		m["_id"], b.Id = bid, bid.Hex()
		return b, d.db.C(Booking).Insert(m)
	}
	b.Created = 0
	m, bid := make(bson.M), bson.ObjectIdHex(b.GetId())
	helper.ConveterI2M(b, m)
	return b, d.db.C(Booking).UpdateId(bid, bson.M{"$set": m})
}

func (d *DB) DeleteBooking(id, accountId, customerId string) error {
	if !bson.IsObjectIdHex(id) {
		return errors.New("id is not hex")
	}
	return d.db.C(Booking).Remove(bson.M{"_id": bson.ObjectIdHex(id), "account_id": accountId, "customer_id": customerId})
}
