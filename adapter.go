package main

import (
	"net"
	"os"
	"time"

	"gitlab.com/my0sot1s/booking/db"
	"gitlab.com/my0sot1s/header/seat"

	"gitlab.com/my0sot1s/helper"
	"gitlab.com/my0sot1s/redite"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type IDB interface {
	ReadCustomer(*seat.CustomerRequest) (*seat.Customer, error)
	ReadCustomers(*seat.CustomerRequest) ([]*seat.Customer, error)
	UpsertCustomer(*seat.Customer) (*seat.Customer, error)
	DeleteCustomer(string, string) error
	ReadBooking(*seat.BookingRequest) (*seat.Booking, error)
	ReadBookings(*seat.BookingRequest) ([]*seat.Booking, error)
	UpsertBooking(*seat.Booking) (*seat.Booking, error)
	DeleteBooking(string, string, string) error
}

type ICache interface {
	SetValue(string, string, time.Duration) error
	GetValue(string) (string, error)
	DelKey([]string) (int, error)
	LPushItem(string, int, ...interface{}) error
	LRangeAll(string) ([]map[string]interface{}, error)
	SetExpired(string, int) bool
}

// InitDB just init
func InitDB(dbURL, dbName string) (*db.DB, error) {
	dbx := &db.DB{}
	err := dbx.InitDB(dbName, dbURL)
	return dbx, err
}

// InitCache just init
func InitCache(rdHost, rdPw string) (*redite.RedisCli, error) {
	rd := &redite.RedisCli{}
	err := rd.InitRd(rdHost, rdPw)
	return rd, err
}

// InitGrpc create auth server
func InitGrpc(port string) error {
	listen, err := net.Listen("tcp", port)
	if err != nil {
		return err
	}
	// make core
	db, err := InitDB(os.Getenv("DB_URL"), os.Getenv("DB_NAME"))
	if err != nil {
		helper.Log(err)
		return err
	}
	cache, err := InitCache(os.Getenv("RD_HOST"), os.Getenv("RD_PW"))
	if err != nil {
		return err
	}
	helper.Log("Booking grpc running at ", port)
	serve := grpc.NewServer()
	seat.RegisterSeatRPCServer(serve, &Seating{
		db:    db,
		cache: cache,
	})
	reflection.Register(serve)
	return serve.Serve(listen)
}
