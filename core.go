package main

import (
	"context"
	"errors"
	"time"

	"gitlab.com/my0sot1s/header/seat"
)

type Seating struct {
	db    IDB
	cache ICache
}

// Create a customer of account
func (s *Seating) CreateCustomer(ctx context.Context, in *seat.Customer) (*seat.Customer, error) {
	in.State = seat.Customer_State_name[0]
	cust := &seat.Customer{
		AccountId: in.GetAccountId(),
		Name:      in.GetName(),
		Email:     in.GetEmail(),
		Phone:     in.GetPhone(),
		Created:   time.Now().UnixNano(),
	}
	// insert db
	return s.db.UpsertCustomer(cust)
}

// Update Customer field
func (s *Seating) UpdateCustomer(ctx context.Context, in *seat.Customer) (*seat.Customer, error) {
	req := &seat.CustomerRequest{Id: in.GetId()}
	if cust, err := s.db.ReadCustomer(req); err != nil && cust.GetId() == "" {
		return nil, errors.New("customer is not existed")
	}
	// field can update
	return s.db.UpsertCustomer(&seat.Customer{
		Id:    in.GetId(),
		Phone: in.GetPhone(),
		Email: in.GetEmail(),
		Name:  in.GetName(),
		Trace: in.GetTrace(),
	})
}

// Read Customer info
func (s *Seating) ReadCustomers(ctx context.Context, in *seat.CustomerRequest) (*seat.CustomerResponse, error) {
	req := &seat.CustomerRequest{Limit: -100, Anchor: in.GetAnchor()}
	if in.GetLimit() != 0 {
		req.Limit = in.Limit
	}
	customers, err := s.db.ReadCustomers(req)
	if err != nil {
		return nil, err
	}
	resp := &seat.CustomerResponse{Anchor: "", Customers: customers}
	if len(customers) == 0 {
		resp.Anchor = ""
	} else if req.GetLimit() > 0 {
		resp.Anchor = customers[len(customers)-1].GetId()
	} else {
		resp.Anchor = customers[0].GetId()
	}
	return resp, nil
}

// Read Customer info
func (s *Seating) ReadCustomer(ctx context.Context, in *seat.CustomerRequest) (*seat.Customer, error) {
	cust, err := s.db.ReadCustomer(&seat.CustomerRequest{Id: in.GetId()})
	if err != nil {
		return nil, err
	}
	return cust, nil
}

// Create a booking for account
func (s *Seating) CreateBooking(ctx context.Context, in *seat.Booking) (*seat.Booking, error) {
	in.State = seat.Booking_State_name[0]
	if in.GetAccountId() == "" {
		return nil, errors.New("missing field account_id")
	}
	if in.GetBranchServiceIds() == nil {
		return nil, errors.New("missing field branch_service_ids")
	}
	if in.GetCustomerId() == "" {
		return nil, errors.New("missing field customer_id")
	}
	if in.GetStartService() == 0 {
		return nil, errors.New("missing field start_service")
	}
	booking := &seat.Booking{
		AccountId:        in.GetAccountId(),
		BranchServiceIds: in.GetBranchServiceIds(),
		CustomerId:       in.GetCustomerId(),
		StartService:     in.GetStartService(),
		Created:          time.Now().UnixNano(),
	}
	// insert db
	return s.db.UpsertBooking(booking)
}

// Update booking fields
func (s *Seating) UpdateBooking(ctx context.Context, in *seat.Booking) (*seat.Booking, error) {
	req := &seat.BookingRequest{Id: in.GetId()}
	if b, err := s.db.ReadBooking(req); err != nil && b.GetId() == "" {
		return nil, errors.New("booking is not existed")
	}
	// field can update
	return s.db.UpsertBooking(&seat.Booking{
		Id:               in.GetId(),
		BranchServiceIds: in.GetBranchServiceIds(),
		StartService:     in.GetStartService(),
		State:            in.GetState(),
	})
}

// Read all booking with filter account
func (s *Seating) ReadBookingWithAccount(ctx context.Context, in *seat.BookingRequest) (*seat.BookingResponse, error) {
	req := &seat.BookingRequest{Limit: -100, Anchor: in.GetAnchor()}
	if in.GetLimit() != 0 {
		req.Limit = in.Limit
	}
	bookings, err := s.db.ReadBookings(req)
	if err != nil {
		return nil, err
	}
	resp := &seat.BookingResponse{Anchor: "", Bookings: bookings}
	if len(bookings) == 0 {
		resp.Anchor = ""
	} else if req.GetLimit() > 0 {
		resp.Anchor = bookings[len(bookings)-1].GetId()
	} else {
		resp.Anchor = bookings[0].GetId()
	}
	return resp, nil
}

// Read all booking with filter account and booking id
func (s *Seating) ReadBooking(ctx context.Context, in *seat.BookingRequest) (*seat.Booking, error) {
	b, err := s.db.ReadBooking(&seat.BookingRequest{
		Id:        in.GetId(),
		AccountId: in.GetAccountId(),
	})
	if err != nil {
		return nil, err
	}
	return b, nil
}
